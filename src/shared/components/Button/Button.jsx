import React from "react";
import "./Button.scss";

const Button = ({text, icon, type, size, callback}) => {
    const classes = ["Button"];
    const buttonIcon = icon ? <span className="ButtonIcon">{icon}</span> : null;
    const buttonText = text ? <span className="ButtonText">{text}</span> : null;

    if (type) classes.push(type);
    if (!text && icon) classes.push("no-hover");
    if (size) classes.push(size);

    const componentClass = classes.join(" ");

    return (
        <a className={componentClass} href="/" onClick={callback}>
            {buttonIcon}
            {buttonText}
        </a>
    )
}

export default Button;