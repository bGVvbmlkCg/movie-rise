import React from "react";
import "./ErrorBanner.scss";
import ErrorImage from "../../../assets/icons/error_cat.png"

const ErrorBanner = () => {
    return (
        <div className="ErrorBanner">
            <img src={ErrorImage} alt=""/>
            <h2>Some error was happened...</h2>
        </div>
    )
}

export default ErrorBanner;