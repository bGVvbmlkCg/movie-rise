import React from "react";
import "./NavbarAuth.css";
import Button from "../../../../shared/components/Button";

const NavbarAuth = () => {
    return (
        <div>
            <Button text="Sign In" type="transparent"/>
            <Button text="Sign Up"/>
        </div>
    )
}

export default NavbarAuth;