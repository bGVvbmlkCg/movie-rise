import React from "react";
import "./Navbar.scss";
import Logo from "../../../../shared/components/Logo";
import NavbarSearch from "../NavbarSearch";
import NavbarAuth from "../NavbarAuth";

const Navbar = () => {
    return (
        <div className="Navbar">
            <Logo />
            <div className="right-bar">
                <NavbarSearch />
                <NavbarAuth />
            </div>
        </div>
    )
}

export default Navbar;